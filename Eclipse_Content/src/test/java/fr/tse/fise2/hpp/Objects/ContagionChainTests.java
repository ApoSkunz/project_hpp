package fr.tse.fise2.hpp.Objects;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class ContagionChainTests {

	@Test
	public void testAddNewPeopleToChain() {
		String idPerson = "2";
		Double currentTime = 1.754567689;

		ContagionChain bufferChainActual = new ContagionChain("0", 1.754567389, "Russia");
		bufferChainActual.addNewPeopleToChain(idPerson, currentTime);

		ContagionChain bufferChainExpected = new ContagionChain("0", 1.754567389, "Russia");

		ArrayList<Double> contagionTimes = new ArrayList<Double>();
		ArrayList<String> idOfContagiousPeople = new ArrayList<String>();
		contagionTimes.add(1.754567389);
		contagionTimes.add(1.754567689);
		idOfContagiousPeople.add("0");
		idOfContagiousPeople.add("2");
		bufferChainExpected.setContagionTimes(contagionTimes);
		bufferChainExpected.setIdOfContagiousPeople(idOfContagiousPeople);
		assertEquals(true, bufferChainExpected.equals(bufferChainActual));
	}

	@Test
	public void testDeleteContagionsTooOld() {
		Double currentTime = 1209600 + 1.754567589;
		String idContaminedBy = "3";
		long bufferTopThree = 30;

		// Case One
		ContagionChain bufferChainActual = new ContagionChain("0", 1.754567389, "Russia");
		bufferChainActual.addNewPeopleToChain("1", 1.754567589); // Too old limit
		bufferChainActual.addNewPeopleToChain("2", 1.754567689);

		ContagionChain bufferChainExpected = new ContagionChain("0", 1.754567689, "Russia");
		ArrayList<String> idContagiousExpected = new ArrayList<String>();
		idContagiousExpected.add("2");
		bufferChainExpected.setIdOfContagiousPeople(idContagiousExpected);
		bufferChainExpected.setContagionScore(4);
		Boolean isAnIdCaseOneExpected = false;
		Boolean isAnIdCaseOneActual = bufferChainActual.deleteContagionsTooOld(currentTime, idContaminedBy,
				bufferTopThree);
		assertEquals(isAnIdCaseOneExpected, isAnIdCaseOneActual);
		assertEquals(true, bufferChainExpected.equals(bufferChainActual));

		// Case Two
		ContagionChain bufferChainTwoActual = new ContagionChain("0", 1.754567389, "Russia");
		bufferChainTwoActual.addNewPeopleToChain("1", 1.754567589); // Too old limit
		bufferChainTwoActual.addNewPeopleToChain("3", 1.754567689);

		ContagionChain bufferChainTwoExpected = new ContagionChain("0", 1.754567689, "Russia");
		ArrayList<String> idContagiousTwoExpected = new ArrayList<String>();
		idContagiousTwoExpected.add("3");
		bufferChainTwoExpected.setIdOfContagiousPeople(idContagiousTwoExpected);
		bufferChainTwoExpected.setContagionScore(4);
		Boolean isAnIdCaseTwoExpected = true;
		Boolean isAnIdCaseTwoActual = bufferChainTwoActual.deleteContagionsTooOld(currentTime, idContaminedBy,
				bufferTopThree);
		assertEquals(isAnIdCaseTwoExpected, isAnIdCaseTwoActual);
		assertEquals(true, bufferChainTwoExpected.equals(bufferChainTwoActual));
	}

	@Test
	public void testDeleteElementsFromJtoZero() {
		ContagionChain bufferChainActual = new ContagionChain("0", 1.754567389, "Russia");
		bufferChainActual.addNewPeopleToChain("1", 1.754567589); // Too old limit
		bufferChainActual.addNewPeopleToChain("2", 1.754567689);
		bufferChainActual.deleteElementsFromJtoZero(1);

		ContagionChain bufferChainExpected = new ContagionChain("0", 1.754567389, "Russia");
		ArrayList<Double> contagionTimes = new ArrayList<Double>();
		ArrayList<String> idOfContagiousPeople = new ArrayList<String>();
		contagionTimes.add(1.754567689);
		idOfContagiousPeople.add("2");
		bufferChainExpected.setContagionTimes(contagionTimes);
		bufferChainExpected.setIdOfContagiousPeople(idOfContagiousPeople);
		assertEquals(true, bufferChainExpected.equals(bufferChainActual));
	}

	@Test
	public void testToStringDisplay() {
		ContagionChain test = new ContagionChain("2", 42, "kazakstan");
		String str = "{ countryName: kazakstan, contagionScore: 10, rootId: 2, chainId: 1, contagionTimes[42.0], idOfContagiousPeople: [] }";
		test.toStringDisplay().equals(str);
	}
}
