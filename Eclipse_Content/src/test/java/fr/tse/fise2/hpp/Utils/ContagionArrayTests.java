package fr.tse.fise2.hpp.Utils;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;

import fr.tse.fise2.hpp.Objects.ContagionChain;

public class ContagionArrayTests {
	
	public boolean equalschain(ArrayList<ContagionChain> chain1, ArrayList<ContagionChain> chain2) {
		Boolean drapAssertEquals = true;
		if (chain1.size() != chain2.size()) {
			drapAssertEquals = false;
			System.out.println("taille");
		} else {
			for (int i = 0; i < chain1.size(); i++) {
				if (!chain2.get(i).equals(chain1.get(i))) {
					drapAssertEquals = false;
					System.out.println("chaine");
				}
			}
		}
		return drapAssertEquals;
	}
	
	@Test
	public void testContagionArray1() { // Adding an element to a current chain when the contagius personn is the rootId
		
		ArrayList<ContagionChain> actualChain = new ArrayList<ContagionChain>();
		
		ContagionChain Chain1 = new ContagionChain("0", 1583056800, "France");
		ContagionChain Chain2 = new ContagionChain("1", 1583661600, "Greece");
		ContagionChain Chain3 = new ContagionChain("2", 1584093600, "England");
		
		actualChain.add(Chain1);
		actualChain.add(Chain2);
		actualChain.add(Chain3);
		
		ArrayList<String> dataOnLineCountry = new ArrayList<String>();
		dataOnLineCountry.add("4"); //0
		dataOnLineCountry.add(""); //1
		dataOnLineCountry.add(""); //2
		dataOnLineCountry.add(""); //3
		dataOnLineCountry.add("1584097200"); //4
		dataOnLineCountry.add("0"); //5
		
		ContagionArray.updateDataContagionChains(actualChain, dataOnLineCountry, "France");
		
		ArrayList<ContagionChain> expectedChain = new ArrayList<ContagionChain>();
		ContagionChain actualChain2 = new ContagionChain("1", 1583661600, "Greece");
		ContagionChain actualChain3 = new ContagionChain("2", 1584093600, "England");
		ContagionChain actualChain1 = new ContagionChain("0", 1583056800, "France");
		actualChain1.addNewPeopleToChain("4", (double)1584097200);
		actualChain1.setContagionScore(14);
		expectedChain.add(actualChain1);
		expectedChain.add(actualChain2);
		expectedChain.add(actualChain3);

		assertEquals(true, equalschain(actualChain,expectedChain));
	}

	@Test
	public void testContagionArray2() { // Adding an element to a current chain when the contagius personn is not the rootId
		
ArrayList<ContagionChain> actualChain = new ArrayList<ContagionChain>();
		
		ContagionChain Chain1 = new ContagionChain("0", 1583056800, "France");
		ContagionChain Chain2 = new ContagionChain("1", 1583661600, "Greece");
		ContagionChain Chain3 = new ContagionChain("2", 1584093600, "England");
		Chain1.addNewPeopleToChain("3", (double)1583056900);

		actualChain.add(Chain1);
		actualChain.add(Chain2);
		actualChain.add(Chain3);
		
		ArrayList<String> dataOnLineCountry = new ArrayList<String>();
		dataOnLineCountry.add("4"); //0
		dataOnLineCountry.add(""); //1
		dataOnLineCountry.add(""); //2
		dataOnLineCountry.add(""); //3
		dataOnLineCountry.add("1584097200"); //4
		dataOnLineCountry.add("0"); //5
		
		ContagionArray.updateDataContagionChains(actualChain, dataOnLineCountry, "France");
		
		ArrayList<ContagionChain> expectedChain = new ArrayList<ContagionChain>();
		ContagionChain actualChain2 = new ContagionChain("1", 1583661600, "Greece");
		ContagionChain actualChain3 = new ContagionChain("2", 1584093600, "England");
		ContagionChain actualChain1 = new ContagionChain("0", 1583056800, "France");
		actualChain1.addNewPeopleToChain("3", (double)1583056900);
		actualChain1.addNewPeopleToChain("4", (double)1584097200);
		actualChain1.setContagionScore(18);
		expectedChain.add(actualChain1);
		expectedChain.add(actualChain2);
		expectedChain.add(actualChain3);
		
		assertEquals(true, equalschain(actualChain,expectedChain));
		
	}

	@Test
	public void testContagionArray3() { // Adding an element to the Array list because the contamination id is not on the list
		
ArrayList<ContagionChain> actualChain = new ArrayList<ContagionChain>();
		
		ContagionChain Chain1 = new ContagionChain("0", 1583056800, "France");
		ContagionChain Chain2 = new ContagionChain("1", 1583661600, "Greece");
		ContagionChain Chain3 = new ContagionChain("2", 1584093600, "England");
		
		actualChain.add(Chain1);
		actualChain.add(Chain2);
		actualChain.add(Chain3);
		
		ArrayList<String> dataOnLineCountry = new ArrayList<String>();
		dataOnLineCountry.add("4"); //0
		dataOnLineCountry.add(""); //1
		dataOnLineCountry.add(""); //2
		dataOnLineCountry.add(""); //3
		dataOnLineCountry.add("1584097200"); //4
		dataOnLineCountry.add("3"); //5
		
		ContagionArray.updateDataContagionChains(actualChain, dataOnLineCountry, "France");
		
		ArrayList<ContagionChain> expectedChain = new ArrayList<ContagionChain>();
		ContagionChain actualChain2 = new ContagionChain("1", 1583661600, "Greece");
		ContagionChain actualChain3 = new ContagionChain("2", 1584093600, "England");
		ContagionChain actualChain1 = new ContagionChain("0", 1583056800, "France");
		ContagionChain actualChain4 = new ContagionChain("4", 1584097200, "France");

		actualChain1.setContagionScore(4);
		expectedChain.add(actualChain1);
		expectedChain.add(actualChain2);
		expectedChain.add(actualChain3);
		expectedChain.add(actualChain4);

		assertEquals(true, equalschain(actualChain,expectedChain));
		
	}

	
@Test
public void testContagionArray4() { // Adding an element to the Array list because the contamination id is not on the list
		
		ArrayList<ContagionChain> actualChain = new ArrayList<ContagionChain>();
		
		ContagionChain Chain1 = new ContagionChain("0", 1583056800, "France");
		ContagionChain Chain2 = new ContagionChain("1", 1583661600, "Greece");
		ContagionChain Chain3 = new ContagionChain("2", 1584093600, "England");
		
		actualChain.add(Chain1);
		actualChain.add(Chain2);
		actualChain.add(Chain3);
		ArrayList<String> dataOnLineCountry = new ArrayList<String>();
		dataOnLineCountry.add("4"); //0
		dataOnLineCountry.add(""); //1
		dataOnLineCountry.add(""); //2
		dataOnLineCountry.add(""); //3
		dataOnLineCountry.add("1584356400"); //4 14 jours apr�s la chain 1 pour la supprimer et en cr�er une autre
		dataOnLineCountry.add("0"); //5
		//1584097200
		ContagionArray.updateDataContagionChains(actualChain, dataOnLineCountry, "France");
		
		ArrayList<ContagionChain> expectedChain = new ArrayList<ContagionChain>();
		ContagionChain actualChain2 = new ContagionChain("1", 1583661600, "Greece");
		actualChain2.setContagionScore(4);
		ContagionChain actualChain3 = new ContagionChain("2", 1584093600, "England");
		ContagionChain actualChain1 = new ContagionChain("4", 1584356400, "France");
		
		expectedChain.add(actualChain2);
		expectedChain.add(actualChain3);
		expectedChain.add(actualChain1);

		assertEquals(true, equalschain(actualChain,expectedChain));
		
		
	}

}
