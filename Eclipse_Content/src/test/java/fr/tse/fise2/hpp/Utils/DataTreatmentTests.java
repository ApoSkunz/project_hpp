package fr.tse.fise2.hpp.Utils;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import org.junit.Test;

import fr.tse.fise2.hpp.Objects.ContagionChain;

public class DataTreatmentTests {

	@Test
	public void testGetInformationOnLine() {
		ArrayList<String> dataOnLineExpected = new ArrayList<String>();
		dataOnLineExpected.add("18767891");
		dataOnLineExpected.add("ok");
		dataOnLineExpected.add("OMG");
		String dataOnLineOriginal = "18767891\n, ok, OMG";
		ArrayList<String> dataOnLineActual = DataTreatment.getInformationOnLine(dataOnLineOriginal);
		assertEquals(dataOnLineExpected, dataOnLineActual);
	}

	public Boolean TopThreeFull() {
		ArrayList<ContagionChain> topThreeExpected = new ArrayList<ContagionChain>();
		ContagionChain elmtThree = new ContagionChain("2", 52.678932567, "Brasil");
		elmtThree.setContagionScore(54);
		topThreeExpected.add(elmtThree);
		ContagionChain elmtFour = new ContagionChain("3", 5.678932567, "Marocco");
		elmtFour.setContagionScore(47);
		topThreeExpected.add(elmtFour);
		ContagionChain elmtOne = new ContagionChain("0", 1.754567389, "Russia");
		elmtOne.setContagionScore(43);
		topThreeExpected.add(elmtOne);

		ArrayList<ContagionChain> arrayToCheck = new ArrayList<ContagionChain>();
		ContagionChain elmtOneToCheck = new ContagionChain("0", 1.754567389, "Russia");
		elmtOneToCheck.setContagionScore(43);
		arrayToCheck.add(elmtOneToCheck);
		ContagionChain elmtTwoToCheck = new ContagionChain("1", 5678.5672567, "China");
		elmtTwoToCheck.setContagionScore(2);
		arrayToCheck.add(elmtTwoToCheck);
		ContagionChain elmtThreeToCheck = new ContagionChain("2", 52.678932567, "Brasil");
		elmtThreeToCheck.setContagionScore(54);
		arrayToCheck.add(elmtThreeToCheck);
		ContagionChain elmtFourToCheck = new ContagionChain("3", 5.678932567, "Marocco");
		elmtFourToCheck.setContagionScore(47);
		arrayToCheck.add(elmtFourToCheck);

		ArrayList<ContagionChain> topThreeActual = DataTreatment.getTopThreeContagionChains(arrayToCheck);
		Boolean drapAssertEquals = true;
		if (topThreeActual.size() != topThreeExpected.size()) {
			drapAssertEquals = false;
		} else {
			for (int i = 0; i < topThreeActual.size(); i++) {
				if (!topThreeExpected.get(i).equals(topThreeActual.get(i))) {
					drapAssertEquals = false;
				}
			}
		}
		return drapAssertEquals;
	}

	public Boolean TopThreeTwo() {
		ArrayList<ContagionChain> topThreeExpected = new ArrayList<ContagionChain>();
		ContagionChain elmtThree = new ContagionChain("2", 52.678932567, "Brasil");
		elmtThree.setContagionScore(54);
		topThreeExpected.add(elmtThree);
		ContagionChain elmtFour = new ContagionChain("3", 5.678932567, "Marocco");
		elmtFour.setContagionScore(47);
		topThreeExpected.add(elmtFour);

		ArrayList<ContagionChain> arrayToCheck = new ArrayList<ContagionChain>();
		ContagionChain elmtThreeToCheck = new ContagionChain("2", 52.678932567, "Brasil");
		elmtThreeToCheck.setContagionScore(54);
		arrayToCheck.add(elmtThreeToCheck);
		ContagionChain elmtFourToCheck = new ContagionChain("3", 5.678932567, "Marocco");
		elmtFourToCheck.setContagionScore(47);
		arrayToCheck.add(elmtFourToCheck);

		ArrayList<ContagionChain> topThreeActual = DataTreatment.getTopThreeContagionChains(arrayToCheck);
		Boolean drapAssertEquals = true;
		if (topThreeActual.size() != topThreeExpected.size()) {
			drapAssertEquals = false;
		} else {
			for (int i = 0; i < topThreeActual.size(); i++) {
				if (!topThreeExpected.get(i).equals(topThreeActual.get(i))) {
					drapAssertEquals = false;
				}
			}
		}
		return drapAssertEquals;
	}

	public Boolean TopThreeOne() {
		ArrayList<ContagionChain> topThreeExpected = new ArrayList<ContagionChain>();
		ContagionChain elmtThree = new ContagionChain("2", 52.678932567, "Brasil");
		elmtThree.setContagionScore(54);
		topThreeExpected.add(elmtThree);

		ArrayList<ContagionChain> arrayToCheck = new ArrayList<ContagionChain>();
		ContagionChain elmtThreeToCheck = new ContagionChain("2", 52.678932567, "Brasil");
		elmtThreeToCheck.setContagionScore(54);
		arrayToCheck.add(elmtThreeToCheck);

		ArrayList<ContagionChain> topThreeActual = DataTreatment.getTopThreeContagionChains(arrayToCheck);
		Boolean drapAssertEquals = true;
		if (topThreeActual.size() != topThreeExpected.size()) {
			drapAssertEquals = false;
		} else {
			for (int i = 0; i < topThreeActual.size(); i++) {
				if (!topThreeExpected.get(i).equals(topThreeActual.get(i))) {
					drapAssertEquals = false;
				}
			}
		}
		return drapAssertEquals;
	}

	public Boolean TopThreeZero() {
		ArrayList<ContagionChain> topThreeExpected = new ArrayList<ContagionChain>();

		ArrayList<ContagionChain> arrayToCheck = new ArrayList<ContagionChain>();

		ArrayList<ContagionChain> topThreeActual = DataTreatment.getTopThreeContagionChains(arrayToCheck);
		Boolean drapAssertEquals = true;
		if (topThreeActual.size() != topThreeExpected.size()) {
			drapAssertEquals = false;
		} else {
			for (int i = 0; i < topThreeActual.size(); i++) {
				if (!topThreeExpected.get(i).equals(topThreeActual.get(i))) {
					drapAssertEquals = false;
				}
			}
		}
		return drapAssertEquals;
	}

	@Test
	public void testGetTopThreeContagionChains() {
		Boolean topThreeActual = TopThreeFull();
		Boolean topTwoActual = TopThreeTwo();
		Boolean topOneActual = TopThreeOne();
		Boolean topZeroActual = TopThreeZero();

		assertEquals(true, topThreeActual);
		assertEquals(true, topTwoActual);
		assertEquals(true, topOneActual);
		assertEquals(true, topZeroActual);
	}

	@Test
	public void testGetCountryLowerId() {
		String franceExpected = "France";
		String spainExpected = "Spain";
		String italyExpected = "Italy";

		String franceActual = DataTreatment.getCountryLowerId(1, 2, 3);
		String spainActual = DataTreatment.getCountryLowerId(2, 1, 3);
		String italyActual = DataTreatment.getCountryLowerId(3, 2, 1);
		assertEquals(franceExpected, franceActual);
		assertEquals(spainExpected, spainActual);
		assertEquals(italyExpected, italyActual);
	}
}
