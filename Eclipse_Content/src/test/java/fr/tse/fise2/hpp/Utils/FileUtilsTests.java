package fr.tse.fise2.hpp.Utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

public class FileUtilsTests {
	@Test
	public void writeResultsTest() throws IOException {
		String pathFile = new FileUtilsTests().getClass().getClassLoader()
				.getResource("ExpectedResults/contagion20.csv").getFile();
		FileUtils.writeResults(20, pathFile);
		List<List<String>> actual = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(
				new FileUtils().getClass().getClassLoader().getResource("Results/contagion20.csv").getFile()))) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] values = line.split(",");
				actual.add(Arrays.asList(values));
			}
		}

		List<List<String>> expected = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(
				new FileUtils().getClass().getClassLoader().getResource("Results/contagion20.csv").getFile()))) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] values = line.split(",");
				expected.add(Arrays.asList(values));
			}
		}

		assertEquals(actual, expected);
	}
}
