package fr.tse.fise2.hpp.Utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TimeUtilsTests {

	@Test
	public void testDifferenceBetweeenTwoTimes() {
		Double expectedDiff = (double) 2;
		Double time1 = (double) 0;
		Double time2 = (double) (3600 * 24 * 2);
		Double actualDiff = TimeUtils.differenceBetweeenTwoTimes(time1, time2);
		assertEquals(expectedDiff, actualDiff);
	}

	@Test
	public void testIsToOld() {
		boolean expectedBool1 = false;
		boolean expectedBool2 = true;
		boolean expectedBool3 = true;
		Double time1 = (double) 0;
		Double time2 = (double) (3600 * 24 * 2);
		Double time3 = (double) (3600 * 24 * 14);
		Double time4 = (double) (3600 * 24 * 15);
		boolean actualBool1 = TimeUtils.isTooOld(time1, time2);
		boolean actualBool2 = TimeUtils.isTooOld(time1, time3);
		boolean actualBool3 = TimeUtils.isTooOld(time1, time4);
		assertEquals(expectedBool1, actualBool1);
		assertEquals(expectedBool2, actualBool2);
		assertEquals(expectedBool3, actualBool3);
	}

	@Test
	public void testIsMoreSevenDayOld() {
		boolean expectedBool1 = false;
		boolean expectedBool2 = false;
		boolean expectedBool3 = true;
		Double time1 = (double) 0;
		Double time2 = (double) (3600 * 24 * 2);
		Double time3 = (double) (3600 * 24 * 7);
		Double time4 = (double) (3600 * 24 * 15);
		boolean actualBool1 = TimeUtils.isTooOld(time1, time2);
		boolean actualBool2 = TimeUtils.isTooOld(time1, time3);
		boolean actualBool3 = TimeUtils.isTooOld(time1, time4);
		assertEquals(expectedBool1, actualBool1);
		assertEquals(expectedBool2, actualBool2);
		assertEquals(expectedBool3, actualBool3);
	}

}
