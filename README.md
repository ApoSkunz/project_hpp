# Coronavirus - HPP Project
### Alexandre ANASTASSIADES - Benjamin OLLÉ - Alexandre SOLANE - Hugo Veron

Project status : the data processing is operationnal, we read results in different
files. However, it is not yet optimized

## Status

### Context

We can currently identify the top-3 more important chains of contamined person
for 3 countries. Nevertheless, we can determine how many countries there are(n) to 
analyse the data for n countries f.eg.

### Input Data

We analyse the contagion chains for 3 countries : France, Spain and Italy.
The only fields we analyse are personId, diagnosedTs and contaminated_by.

However, we currently determine the order to read the right file with an arrayList<String>, we
are going to use another way to determine idCountry to read with personn_id field.

### Input streams

We remove special characters and split the lines on the ', ' pattern in an ArrayList<String>
called dataOnLine.

### Output streams

We store the output stream, which corresponds to the current top-3 contagion chains in a 
csv file called contagionN, where N represents the number of person we have to analyse. 
In our example, it's 20, 5.000 or 1.000.000.

Moreover we have decided to display a top 1, top 2 or top 3 if there are not enough 
contagion chains.

### Example

```
Italy	6	10	 Only TOP 1
Spain	3	10	 Only TOP 1
Spain	3	10	 Only TOP 1
France	5	10	 Only TOP 1
France	9	10	 Only TOP 1
France	9	10	 Italy	10	10	 Only TOP 2
France	9	10	 Spain	8	10	 Italy	10	4	TOP 3
```

A data is composed of the country name, the root id of the contagion chain and its
contagion chain score.

## Are the rules followed ?

Rule 0: Yes we are a team of 4 person and we are running our maven Java project 
on Java 8.

Rule 1: determine order with 3 BufferedReader

Rule 2: multi-threading is not implemented yet --> so we have only 1 thread

Rule 3: our algorithm manages contagion by frontiers even if there isn't any case in
3 csv files

Rule 4: chains with a score of 0 have been removed from our storage data.

Rule 5: no problem we store the time as a Float (we should change to date type or Double) !!!!

Rule 6: data are stored by oldest age --> and our top-3 is written in the same 
order

<em>All rules have been followed</em>

## JOB Status

1- Unit Tests have been implemented for each method used by our algorithm
--> Moreover we have checked our results for the case N=20 and checked if the top 1 in
the algorithm with N=5000 is the real chain
2- Architecture by package and some classes
3- Tests have been runned
4- Solution is valid but not half-optimized yet
5- 3 bufferReader for each files have been instancied --> to return to the right line

## Project delivered

- A- Technical Documentation in writting
- B- on the 25th May

## Performance analysis

<strong>We recorded performances of our algorithm while we were sleeping(by recording our screen) for the case
where N = 1.000.000. We recorded the estimated Time each 10.000 iterations to draw this
graph :</strong>

![Main Graph](resources/mainGraph.png?raw=true "Main Graph")

	Execution time measured during the running of our algorithm
	by different conditions and step of our optimization

![Execution Time](resources/executionTime.png?raw=true "Execution time")

	Execution time measured during the running of our algorithm
	
![Complexity Model](resources/processingComplexityEstimation.png?raw=true "Complexity Model")
	
	Complexity model of our execution time is : O(ln(n).n²)

### Remarks : 
So, when we are analysing our algorithm we can say that the complexity is
in O(n³) in the worst case, but in reality we removed with theses file, the
complexity is near to O(ln(n).n²) because we needn't to browse every contagion
chain.
With an i5 2019 processor, we run the algorithm in 3:07:44 (h:min:s), so it's
pretty good, but we think that we can do better with solutions which have been
proposed in our defense.



